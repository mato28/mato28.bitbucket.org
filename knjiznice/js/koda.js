
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 var podatk=[  
     {
        ime:"Rok",
        priimek:"Novak",
        rojstvo:"1938-10-30T14:58",
        diaTlak:120,
        sisTlak:90,
        datumMeritve:"2000-10-30T14:58"
     },
     {
         ime:"Janez",
        priimek:"Matjaz",
        rojstvo:"1998-02-10T18:14",
        diaTlak:135,
        sisTlak:100,
        datumMeritve:"2000-10-30T14:58"
     },
     {
         ime:"Martin",
        priimek:"Cuznar",
        rojstvo:"1968-05-17T11:08",
        diaTlak:100,
        sisTlak:70,
        datumMeritve:"2000-10-30T14:58"
     }
     
     ]
      
      
 function generirajNovePodatke(){
     var x=document.getElementById("izpisZnakov");
     var y=document.getElementById("vnosTlaka");
     for(var i=0;i<3;i++){
        var ehrID= generirajPodatke(i);
         
        var option = document.createElement("option");
        option.text = podatk[i].ime+" "+podatk[i].priimek;
        option.value=ehrID;
        x.add(option);
        var option = document.createElement("option");
        option.text = podatk[i].ime+" "+podatk[i].priimek;
        option.value=ehrID;
        y.add(option);
       
     }
     
 }
 
function generirajPodatke(stPacienta) {
  ehrId = "";

  // TODO: Potrebno implementirati
  sessionID=getSessionId();
  
  var ime=podatk[stPacienta].ime;
  var priimek=podatk[stPacienta].priimek;
  var datumRojstva=podatk[stPacienta].rojstvo;
  var datumMeritve=podatk[stPacienta].datumMeritve;
  var diastolicniKrvniTlak=podatk[stPacienta].diaTlak;
  var sistolicniKrvniTlak=podatk[stPacienta].sisTlak;
  var merilec="medicinska Sestra";
  
  
    $.ajaxSetup({
      headers: {"Ehr-Session": sessionID}
	});
    
    $.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		     $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#generirajPodatkeSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
                            $("#izpisEHRID").val(ehrId);
		                   
		                
		                var podatki = {
		            	// Struktura predloge je na voljo na naslednjem spletnem naslovu:
                    // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumMeritve,
		    "vital_signs/height_length/any_event/body_height_length": 180,
		    "vital_signs/body_weight/any_event/body_weight": 80,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": 36,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": 22
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#generirajPodatkeSporocilo").html(
              "<span class='obvestilo label label-success fade-in'> Uspesno generirani podatki\
              .</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
		
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	
   
  return ehrId;
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija


function dodajanjeMeritev(){
    sessionId=getSessionId();
    var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
	var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();
	var merilec = $("#dodajVitalnoMerilec").val();
	
	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": 180,
		    "vital_signs/body_weight/any_event/body_weight": 80,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": 36,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": 20
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});

	}
}

function izpisPodatkov() {
	sessionId = getSessionId();

	var ehrId = $("#izpisEHRID").val();
	//var zacetniDatum = $("#izpisOd").val();
//	var koncniDatum = $("#izpisDo").val();

	if (!ehrId || ehrId.trim().length == 0 ) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#opis").html("<br/><span>Izpis " +
          "podatkov za </b> bolnika <b>'" + party.firstNames +
          " " + party.lastNames + "'</b>.</span><br/><br/>");
			
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	var result= "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Krvni tlak</th></tr>";
					    	for(var i in res){
					    		result+="<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + res[i].diastolic +
                          " " + res[i].systolic + "</td>";
					    	}
					    	result += "</table>";
						     $("#podatki").append(result);
					    	
					    }
					});
		
	    	}
					
		});
	}
}


$('#izpisZnakov').change(function(){

	var a=$('#izpisZnakov').val();
	document.getElementById("izpisEHRID").value=a;
});
$('#vnosTlaka').change(function(){
document.getElementById("dodajVitalnoEHR").value=$('#vnosTlaka').val();
});
function spremeni(){
		var a=document.getElementById('izpisZnakov').value;
	document.getElementById("izpisEHRID").value=a;
	
	
}
function spremeni1(){
		var a=document.getElementById('vnosTlaka').value;
	document.getElementById("dodajVitalnoEHR").value=a;
	
}